<!doctype html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta charset="utf-8">
<meta name="description" content="WaWeb - Public web portal for Washington WebApps">
<meta name="author" content="Aaron R Miller &#x3C;amiller@washingtonwebapps.com&#x3E;">

<asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
<title><g:layoutTitle default="WaWeb" /></title>

<!-- Custom fonts for this theme -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

<asset:stylesheet src="application.css" />

<g:layoutHead />
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top"> Washington WebApps
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button"
        data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false"
        aria-label="Toggle navigation">
        Menu <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ml-auto">
          <li class="nav-item"><a class="nav-link js-scroll-trigger"
            href="#services">Services</a></li>
          <li class="nav-item"><a class="nav-link js-scroll-trigger"
            href="#portfolio">Portfolio</a></li>
          <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">About</a></li>
          <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#team">Team</a></li>
          <li class="nav-item"><a class="nav-link js-scroll-trigger"
            href="#contact">Contact</a></li>
        </ul>
      </div>
    </div>
  </nav>

  <g:layoutBody />

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <span class="copyright">Copyright &copy; Your Website 2018</span>
        </div>
        <div class="col-md-4">
          <ul class="list-inline social-buttons">
            <li class="list-inline-item"><a href="#"> <i class="fab fa-twitter"></i>
            </a></li>
            <li class="list-inline-item"><a href="#"> <i
                class="fab fa-facebook-f"></i>
            </a></li>
            <li class="list-inline-item"><a href="#"> <i
                class="fab fa-linkedin-in"></i>
            </a></li>
          </ul>
        </div>
        <div class="col-md-4">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item"><a href="#">Privacy Policy</a></li>
            <li class="list-inline-item"><a href="#">Terms of Use</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <asset:javascript src="application.js" />

</body>
</html>
